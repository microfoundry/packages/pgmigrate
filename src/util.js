import {
    concat,
    curry,
    head,
    isEmpty,
    length,
    tail
} from "ramda";
import { join as pathjoin } from "path";
import { native as pg } from "pg";
import Promise, { promisify } from "bluebird";

const pgUser  = process.env.POSTGRES_USER || "postgres";
const pgPwd   = process.env.POSTGRES_PASSWORD || "password";
const pgHost  = process.env.POSTGRES_HOST || "postgres";
const pgDB    = process.env.POSTGRES_DATABASE || "postgres";
const dbUrl   = `postgres://${pgUser}:${pgPwd}@${pgHost}/${pgDB}`;

console.log("Connecting to " + dbUrl);

const client = new pg.Client(dbUrl);
const connect = promisify(::client.connect);

export const sleep = (timeout) => {
    return new Promise((resolve) => {
        setTimeout(resolve, timeout);
    });
};

export const abspath = (path) => {
    if (path[0] === "/") return path;
    return pathjoin(process.cwd(), path);
};

export const tableExists = async (query, table) => {
    const result = await query(`
        SELECT EXISTS (
            SELECT 1
            FROM   information_schema.tables
            WHERE  table_schema = 'public'
            AND    table_name = '${table}'
        );
    `);
    return result.rows[0].exists === true;
};

export const indexExists = async (query, index) => {
    const result = await query(`
        SELECT * FROM pg_indexes
        WHERE indexname ~ '${index}';
    `);
    return length(result.rows) > 0;
};

export const select = async (queryFn, statement) => {
    const result = await queryFn(statement);
    return result.rows;
};

export const waitForConnection = async (attempts = 20, interval = 500) => {
    if (attempts <= 0) throw new Error("Failed to connect");
    try {
        console.log("attempting to connect to: ", dbUrl);
        await connect();
        client.end();
    } catch (e) {
        await sleep(interval);
        await waitForConnection(--attempts, interval);
    }
};

export const mapSerialAsync = curry(async (fn, list) => {
    if (!list || list.length === 0) return [];
    let result = await fn(head(list));
    let rest = tail(list);
    let results = [];

    if (rest.length > 0) {
        results = await mapSerialAsync(fn, rest);
        return concat([result], results);
    } else {
        return [result];
    }
});

export const reduceAsync = curry(async (fn, acc, list) => {
    if (isEmpty(list)) return acc;
    acc = await fn(acc, head(list));
    let rest = tail(list);
    if (isEmpty(rest)) {
        return acc;
    } else {
        return await reduceAsync(fn, acc, rest);
    }
});
