import program from "commander";
import { createMigration, runMigrate } from "../src/pgmigrate";
import packageInfo from "../package.json";

console.log(`pgmigrate ${packageInfo.version}`);

export const run = () => {
  program
      .command("new <name>")
      .action(createMigration);

  program
      .command("run")
      .action(runMigrate);

  program.parse(process.argv);
};
