import pg from "pg";
import moment from "moment";
import mkdirp from "mkdirp-bluebird";
import { readdir as _readdir, writeFile as _writeFile } from "fs";
import { promisify } from "bluebird";
import { abspath, mapSerialAsync, select, waitForConnection } from "./util";
import {
    compose,
    equals,
    find,
    findIndex,
    gt,
    isEmpty,
    last,
    map,
    match,
    nth,
    slice,
    sort,
    test
} from "ramda";

const MIGRATIONS_TABLE = "schemata";
const MIGRATIONS_DIR   = "./migrations";

const readdir = promisify(_readdir);
const writeFile = promisify(_writeFile);

const createMigrationsTable = async (query, migrationsTable) => {
    try {
        await query(`
            create table if not exists ${migrationsTable} (
                version varchar(20)
            );
        `);
        const result = await query(`select count(*) from ${migrationsTable}`);
        if (result.rows[0].count === "0") {
            await query(`insert into ${migrationsTable} values ('0')`);
        }
    } catch (err) {
        if (!match(/already exists/, err.message)) {
            console.log("error creating migrations table:", err);
            throw err;
        }
    }
};

export const migrate = async (db, opts = {}) => {
    try {
        const migrationsTable   = opts.migrationTable || MIGRATIONS_TABLE;
        const migrationsDir     = opts.migrationsDir || MIGRATIONS_DIR;
        const migrationVersion  = compose(nth(1), match(/^(\d+)_/));
        const query             = promisify(::db.query);

        await createMigrationsTable(query, migrationsTable);

        const result            = await query(`select version from ${migrationsTable}`);
        const dbVersion         = result.rows[0].version;
        const migrationFiles    = sort(gt, await readdir(migrationsDir));
        const migrationVersions = map(migrationVersion, migrationFiles);
        const startIndex        = findIndex(equals(dbVersion), migrationVersions) + 1;
        const toRun             = slice(startIndex, Infinity, migrationFiles);

        const execMigration = async (file) => {
            console.log(file);
            const path = abspath(`${migrationsDir}/${file}`);
            const module = require(path);
            const up = module.up;
            return await query(up());
        };

        await mapSerialAsync(execMigration, toRun);

        if (!isEmpty(toRun)) {
            const newVersion = migrationVersion(last(toRun));
            await query(`update ${migrationsTable} set version='${newVersion}'`);
        }
    } catch (err) {
        console.log("err:", err); // eslint-disable-line
        throw err;
    }
};

export const rollback = async (db, opts = {}) => {
    const migrationsTable   = opts.migrationTable || MIGRATIONS_TABLE;
    const migrationsDir     = opts.migrationsDir || MIGRATIONS_DIR;
    const query             = promisify(::db.query);
    const migrationFiles    = sort(gt, await readdir(migrationsDir));
    const migrationVersion  = compose(nth(1), match(/^(\d+)_/));
    const migrationVersions = map(migrationVersion, migrationFiles);
    const currentVersion    = (await select(query, "select version from schemata"))[0].version;
    const currentIndex      = findIndex(equals(currentVersion), migrationVersions);

    if (currentIndex === -1) throw new Error("Current version not found in list of migration files");

    const previousVersion = (currentIndex === 0) ? "0" : nth(currentIndex - 1, migrationVersions);
    const currentMigration = find(test(new RegExp(`^${currentVersion}`)), migrationFiles);
    const path = `${migrationsDir}/${currentMigration}`;
    const module = require(path);
    const down = module.down;
    await query(down());

    query(`update ${migrationsTable} set version = '${previousVersion}'`);
};

const newMigration = `
export const up = () => \`
-- [up sql here]
\`;

export const down = () => \`
-- [down sql here]
\`;
`;

export const createMigration = async (name) => {
    const time = moment().format("YYYYMMDDHHMMss");
    const dir = "./migrations";
    const path = `${dir}/${time}_${name}.js`;
    await mkdirp(dir);
    await writeFile(path, newMigration, "utf8");
    console.log(`Wrote new migration: ${path}`);
};

export const runMigrate = async () => {
    try {
        console.log("Running migrations");
        const pgUser  = process.env.POSTGRES_USER || "postgres";
        const pgPwd   = process.env.POSTGRES_PASSWORD || "password";
        const pgHost  = process.env.POSTGRES_HOST || "postgres";
        const pgDB    = process.env.POSTGRES_DATABASE || "postgres";
        const dbUrl   = `postgres://${pgUser}:${pgPwd}@${pgHost}/${pgDB}`;
        const db      = new pg.Client(dbUrl);
        const connect = promisify(::db.connect);

        await waitForConnection();
        await connect();
        await migrate(db);
        db.end();
    } catch (e) {
        console.error(e);
        process.exit(1);
    }
};
