import pg from "pg";
import { promisify } from "bluebird";
import { select, abspath, tableExists, indexExists } from "../src/util";
import { migrate, rollback } from "../src/pgmigrate";

const dbUrl = "postgres://postgres:password@postgres/postgres";
let db      = null;
let connect = null;
let query   = null;

before(async () => {
    db = new pg.Client(dbUrl);
    connect = promisify(::db.connect);
    query = promisify(::db.query);
    await connect();
});

after(() => db.end());

beforeEach(async () => {
    await query("start transaction");
});

afterEach(async () => {
    await query("rollback");
});

describe("pgmigrate", () => {
    describe("single", () => {
        const migrationsDir = abspath("test/fixtures/migrations/single");

        it("should run a migration", async () => {
            await migrate(db, { migrationsDir });
            const schemataExists = await tableExists(query, "schemata");
            const usersExists = await tableExists(query, "users");
            const currentVersion = (await select(query, "select version from schemata"))[0].version;
            expect(schemataExists).to.eql(true);
            expect(usersExists).to.eql(true);
            expect(currentVersion).to.eql("20160601000000");
        });

        it ("shouldn't run migrations", async () => {
            await query("create table if not exists schemata (version varchar(20))");
            await query("insert into schemata values ('20160601000000')");
            await migrate(db, { migrationsDir });
            const usersExists = await tableExists(query, "users");
            expect(usersExists).to.eql(false);
        });

        it("should rollback", async () => {
            await migrate(db, { migrationsDir });
            await rollback(db, { migrationsDir });
            const currentVersion = (await select(query, "select version from schemata"))[0].version;
            const usersExists = (await tableExists(query, "users"));
            expect(currentVersion).to.eql("0");
            expect(usersExists).to.eql(false);
        });
    });

    describe("multiple", () => {
        const migrationsDir = abspath("test/fixtures/migrations/multiple");

        it("should run the migrations", async () => {
            await migrate(db, { migrationsDir });

            const schemataExists = await tableExists(query, "schemata");
            const usersExists = await tableExists(query, "users");
            const fbIdIdxExists = await indexExists(query, "users_fb_id_idx");
            const emailIdxExists = await indexExists(query, "users_email_idx");
            const currentVersion = (await select(query, "select version from schemata"))[0].version;

            expect(schemataExists).to.eql(true);
            expect(usersExists).to.eql(true);
            expect(fbIdIdxExists).to.eql(true);
            expect(emailIdxExists).to.eql(true);
            expect(currentVersion).to.eql("20160603000000");
        });

        it("shouldn't run migrations once they've been run");

        it("should run migrations outstanding migrations need to run");

        it("should rollback");
    });
});
