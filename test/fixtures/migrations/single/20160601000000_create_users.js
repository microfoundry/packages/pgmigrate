
export const up = () => `
create table users (
    id serial primary key,
    fb_id varchar (15) not null,
    email varchar (50) not null,
    name varchar (100)
);
`;

export const down = () => `
drop table users;
`;
