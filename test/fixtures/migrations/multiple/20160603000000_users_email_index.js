
export const up = () => `
create index users_email_idx on users (email);
`;

export const down = () => `
drop index users_email_idx;
`;
