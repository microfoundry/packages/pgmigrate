
export const up = () => `
create index users_fb_id_idx on users (fb_id);
`;

export const down = () => `
drop index users_fb_id_idx;
`;
