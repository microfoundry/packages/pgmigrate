#!/usr/bin/env node

// See here: https://stackoverflow.com/questions/35120305/using-babel-register-in-my-cli-npm-app-works-locally-but-not-globally-after-pub
require("babel-register")({ ignore: false, only: /(src|migrations)/ });
require("babel-polyfill");
require("../src/cli").run();
